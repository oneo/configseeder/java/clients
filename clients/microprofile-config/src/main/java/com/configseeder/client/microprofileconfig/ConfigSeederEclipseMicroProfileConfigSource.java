/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.microprofileconfig;

import com.configseeder.client.ConfigSeederClient;
import com.configseeder.client.ConfigSeederClientConfiguration;
import com.configseeder.client.model.Identification;
import java.util.logging.Level;
import lombok.extern.java.Log;

/**
 * Microprofile Config Configuration Source.
 */
@Log
public class ConfigSeederEclipseMicroProfileConfigSource extends ConfigSeederBaseConfigSource {

    // Name of this ConfigSource
    private static final String CONFIGSEEDER_CONFIG_SOURCE_NAME = "com.configseeder.client.configsource.eclipsemicroprofile-config";

    private static final Object lock = new Object();
    private ConfigSeederClient configSeederClient;
    private static boolean initializing = false;

    /**
     * Default initialization.
     */
    public ConfigSeederEclipseMicroProfileConfigSource() {
        super(CONFIGSEEDER_CONFIG_SOURCE_NAME, 200);
    }

    /**
     * Direct initialization of a config seeder client.
     *
     * @param configSeederClient config seeder client
     */
    ConfigSeederEclipseMicroProfileConfigSource(ConfigSeederClient configSeederClient) {
        this();
        this.configSeederClient = configSeederClient;
    }

    @Override
    public int getOrdinal() {
        return 200;
    }

    @Override
    protected ConfigSeederClient getConfigSeederClient() {
        if (configSeederClient != null || initializing) {
            return configSeederClient;
        }
        synchronized (lock) {
            if (configSeederClient != null) {
                return configSeederClient;
            }

            initializing = true;

            try {
                ConfigSeederClientConfigurationProvider clientConfigurationProvider = new ConfigSeederClientConfigurationProvider();
                ConfigSeederClientConfiguration clientConfiguration = clientConfigurationProvider.getClientConfiguration();
                if (clientConfiguration != null) {
                    clientConfiguration.validate();
                    configSeederClient = new ConfigSeederClient(clientConfiguration, Identification.create("EclipseMicroProfileConfig"));
                }
            } catch (Exception ex) {
                log.log(Level.WARNING, "Unable to start yet ConfigSeederClient: " + ex.getMessage(), ex);
            }

            initializing = false;

            return configSeederClient;
        }
    }

    @Override
    public void close() {
        synchronized (lock) {
            if (configSeederClient != null) {
                ConfigSeederClient reference = this.configSeederClient;
                configSeederClient = null;
                reference.shutdown();
            }
        }
    }

}
