/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.client.microprofileconfig;

import lombok.Value;

@Value
public class KeyValue {

    String key;
    String value;

}
