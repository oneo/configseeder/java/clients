/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.client.microprofileconfig;

import com.configseeder.client.ConfigSeederClient;
import java.io.Closeable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import lombok.extern.java.Log;
import org.eclipse.microprofile.config.spi.ConfigSource;

/**
 * Common implementation for all ConfigSources.
 */
@Log
public abstract class ConfigSeederBaseConfigSource implements ConfigSource, Serializable, Closeable {

    private final int ordinal;
    private final String name;

    public ConfigSeederBaseConfigSource(String name, int ordinal) {
        log.log(Level.INFO, "ConfigSource " + name + " registered");
        this.name = name;
        this.ordinal = ordinal;
    }

    public int getOrdinal() {
        return this.ordinal;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.getName();
    }

    protected abstract ConfigSeederClient getConfigSeederClient();

    @Override
    public Set<String> getPropertyNames() {
        final Set<String> propertyNames = new HashSet<>();
        try {
            ConfigSeederClient client = getConfigSeederClient();
            if (client != null) {
                propertyNames.addAll(client.getKeys());
            }
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        return propertyNames;
    }

    @Override
    public Map<String, String> getProperties() {
        final Map<String, String> properties = new HashMap<>();
        try {
            ConfigSeederClient client = getConfigSeederClient();
            if (client != null) {
                properties.putAll(client.getProperties());
            }
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        return properties;
    }

    @Override
    public String getValue(final String key) {
        if (key.startsWith("configseeder") || (key.startsWith("%") && key.contains("configseeder"))) {
            return null;
        }
        try {
            ConfigSeederClient client = getConfigSeederClient();
            if (client != null) {
                return client.getString(key);
            }
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void close() {
        ConfigSeederClient client = getConfigSeederClient();
        if (client != null) {
            client.shutdown();
        }
    }

}
