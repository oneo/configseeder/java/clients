/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.ConfigurationValueType;
import com.configseeder.client.model.Identification;
import com.configseeder.client.util.JsonValueMapper;
import com.configseeder.shared.model.ComparableVersion;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpConnectTimeoutException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.UnsupportedCharsetException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Rest client able to retrieve values from ConfigSeeder.
 */
@Getter
@Setter
class ConfigSeederRestClient {

    static final String HEADER_ACCEPT = "Accept";
    static final String HEADER_CONTENT_TYPE = "Content-Type";
    static final String HEADER_AUTHORIZATION = "Authorization";

    @SuppressWarnings("squid:S1075") // fixed path is ok here
    static final String PATH_TO_WELL_KNOWN = "/.well-known/config-seeder/info";
    @SuppressWarnings("squid:S1075") // fixed path is ok here
    static final String PATH_TO_EXTERNAL_PROPERTIES = "/public/api/v2/configurations";

    public static final String ACCEPT_CONFIGURATION_VALUES = "application/vnd.config-seeder.values-collection.v1+json;charset=utf-8";

    /** Logger. */
    private final Logger logger = new Logger(ConfigSeederRestClient.class);

    private final JsonValueMapper jsonValueMapper = new JsonValueMapper();

    private final String url;
    private Duration timeout;
    private Identification identification;
    private final String apiKey;
    private final HttpClient httpClient;

    ConfigSeederRestClient(String url, String apiKey) {
        this(url, apiKey, Duration.of(2, ChronoUnit.SECONDS), Identification.create("Core"));
    }

    ConfigSeederRestClient(@Nonnull String url, @Nonnull String apiKey,
            @Nonnull Duration timeout, @Nonnull Identification identification) {
        this.timeout = timeout;
        this.identification = identification;
        if (!url.startsWith("http")) {
            throw new IllegalArgumentException("URL '" + url + "' seems not be a valid url");
        }
        if (url.endsWith("/")) {
            this.url = url.substring(0, url.length() - 1);
        } else {
            this.url = url;
        }
        if (apiKey.trim().isEmpty()) {
            throw new IllegalArgumentException("API Key should not be null");
        }
        this.apiKey = apiKey;
        this.httpClient = HttpClient.newBuilder()
                .connectTimeout(timeout)
                .build();
    }

    public ComparableVersion getServerVersion() {
        HttpRequest httpRequest = getRequestBuilder(getWellKnownServerUrl(), "application/json").GET().build();
        return doRequest(httpRequest, PATH_TO_WELL_KNOWN, this::handleWellKnownResponse);
    }

    public ComparableVersion handleWellKnownResponse(HttpResponse<String> response) {
        String body = response.body();
        JsonValue wellKnownJson = Json.parse(body);
        JsonObject wellKnownJsonObject = wellKnownJson.asObject();
        JsonValue appVersionValue = wellKnownJsonObject.get("appVersion");
        return appVersionValue == null ? null : new ComparableVersion(appVersionValue.asString());
    }

    public Map<String, ConfigValue> getProperties(ConfigurationRequest configurationRequest) throws ConfigurationValueServerHttpStatusException {
        configurationRequest.validate();

        List<Pair> localVarQueryParams = new ArrayList<>();
        StringJoiner localVarQueryStringJoiner = new StringJoiner("&");
        localVarQueryParams.addAll(parameterToPairs("tenantKey", configurationRequest.getTenantKey()));
        List<String> configurationGroupKeys = configurationRequest.getConfigurations().stream()
                .map(ConfigurationEntryRequest::getConfigurationGroupKey)
                .filter(s -> s != null && !s.trim().isBlank())
                .collect(Collectors.toList());
        if (!configurationGroupKeys.isEmpty()) {
            localVarQueryParams.addAll(parameterToPairs("multi", "configurationGroupKeys", configurationGroupKeys));
        }
        List<String> configurationGroupIds = configurationRequest.getConfigurations().stream()
                .map(ConfigurationEntryRequest::getConfigurationGroupId)
                .filter(s -> s != null && !s.trim().isBlank())
                .collect(Collectors.toList());
        if (!configurationGroupIds.isEmpty()) {
            localVarQueryParams.addAll(parameterToPairs("multi", "configurationGroupIds", configurationGroupIds));
        }
        localVarQueryParams.addAll(parameterToPairs("environmentKey", configurationRequest.getEnvironmentKey()));
        localVarQueryParams.addAll(parameterToPairs("version", configurationRequest.getVersion()));
        localVarQueryParams.addAll(parameterToPairs("dateTime", configurationRequest.getDateTime()));
        if (configurationRequest.getLabels() != null && !configurationRequest.getLabels().isEmpty()) {
            localVarQueryParams.addAll(parameterToPairs("multi", "labels", configurationRequest.getLabels()));
        } else {
            localVarQueryParams.addAll(parameterToPairs("labels", configurationRequest.getContext()));
        }

        StringJoiner queryJoiner = new StringJoiner("&");
        localVarQueryParams.forEach(p -> queryJoiner.add(p.getName() + '=' + p.getValue()));
        if (localVarQueryStringJoiner.length() != 0) {
            queryJoiner.add(localVarQueryStringJoiner.toString());
        }
        String requestUrl = getConfigurationRequestUrl() + '?' + queryJoiner;

        HttpRequest httpRequest = getRequestBuilder(requestUrl, ACCEPT_CONFIGURATION_VALUES).GET().build();
        return doRequest(httpRequest, PATH_TO_EXTERNAL_PROPERTIES, this::handleConfigurationValuesResponse);
    }

    public Map<String, ConfigValue> handleConfigurationValuesResponse(HttpResponse<String> response) {
        String body = response.body();
        final JsonValue jsonValue = Json.parse(body);
        final JsonArray rootObject = jsonValue.asArray();
        final Map<String, ConfigValue> values = new LinkedHashMap<>();
        for (JsonValue jsonArrayValue : rootObject.values()) {
            final JsonObject jsonObject = jsonArrayValue.asObject();
            final ConfigValue configValue = jsonValueMapper.mapConfigValue(jsonObject);
            values.put(configValue.getKey(), configValue);
        }
        return values;
    }

    private <T> T doRequest(HttpRequest httpRequest, String requestUrl, Function<HttpResponse<String>, T> handler)
            throws ConfigurationValueServerHttpStatusException {
        try {
            HttpResponse<String> response = httpClient.send(httpRequest, withProblemSupportAndStringResponse());
            return handler.apply(response);
        } catch (HttpConnectTimeoutException e) {
            throw new RequestTimedOutException(String.format("Could not connect to '%s' within %s", requestUrl, timeout), e);
        } catch (InterruptedException e) {
            throw new ConfigSeederTechnicalException(String.format("Connection to '%s' was interrupted", requestUrl), e);
        } catch (UnsupportedCharsetException e) {
            throw new ConfigSeederTechnicalException("Could not decode to UTF-8", e);
        } catch (MalformedURLException e) {
            throw new InvalidClientConfigurationException(String.format("URL '%s' is invalid", requestUrl), e);
        } catch (IOException e) {
            if (e.getCause() instanceof ConfigurationValueServerHttpStatusException) {
                throw (ConfigurationValueServerHttpStatusException) e.getCause();
            }
            throw new ConfigSeederTechnicalException("Could not connect and fetch values", e);
        } catch (RuntimeException e) {
            throw new ConfigSeederTechnicalException(String.format("Unable to fetch from URL %s", requestUrl), e);
        }
    }

    HttpRequest.Builder getRequestBuilder(String requestUrl, String acceptHeader) {
        return HttpRequest.newBuilder()
                .uri(URI.create(requestUrl))
                .header(HEADER_AUTHORIZATION, "Bearer " + apiKey)
                .header(Identification.HEADER_X_USER_AGENT, identification.getUserAgent())
                .header(Identification.HEADER_X_HOSTNAME, identification.getHostname())
                .header(Identification.HEADER_X_HOST_IDENTIFICATION, identification.getHostIdentification())
                .header(HEADER_ACCEPT, acceptHeader);
    }

    String getWellKnownServerUrl() {
        return String.format("%s%s", url, PATH_TO_WELL_KNOWN);
    }

    String getConfigurationRequestUrl() {
        return String.format("%s%s", url, PATH_TO_EXTERNAL_PROPERTIES);
    }

    private static String valueToString(Object value) {
        if (value == null) {
            return "";
        }
        if (value instanceof OffsetDateTime) {
            return ((OffsetDateTime) value).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        }
        return value.toString();
    }

    /**
     * URL encode a string in the UTF-8 encoding.
     *
     * @param s String to encode.
     * @return URL-encoded representation of the input string.
     */
    public static String urlEncode(String s) {
        return URLEncoder.encode(s, UTF_8).replaceAll("\\+", "%20");
    }

    /**
     * Convert a URL query name/value parameter to a list of encoded {@link Pair}
     * objects.
     *
     * <p>The value can be null, in which case an empty list is returned.</p>
     *
     * @param name  The query name parameter.
     * @param value The query value, which may not be a collection but may be
     *              null.
     * @return A singleton list of the {@link Pair} objects representing the input
     *         parameters, which is encoded for use in a URL. If the value is null, an
     *         empty list is returned.
     */
    public static List<Pair> parameterToPairs(String name, Object value) {
        if (name == null || name.isEmpty() || value == null) {
            return Collections.emptyList();
        }
        return Collections.singletonList(new Pair(urlEncode(name), urlEncode(valueToString(value))));
    }

    /**
     * Convert a URL query name/collection parameter to a list of encoded
     * {@link Pair} objects.
     *
     * @param collectionFormat The swagger collectionFormat string (csv, tsv, etc).
     * @param name             The query name parameter.
     * @param values           A collection of values for the given query name, which may be
     *                         null.
     * @return A list of {@link Pair} objects representing the input parameters,
     *         which is encoded for use in a URL. If the values collection is null, an
     *         empty list is returned.
     */
    public static List<Pair> parameterToPairs(
            String collectionFormat, String name, Collection<?> values) {
        if (name == null || name.isEmpty() || values == null || values.isEmpty()) {
            return Collections.emptyList();
        }

        // get the collection format (default: csv)
        String format = collectionFormat == null || collectionFormat.isEmpty() ? "csv" : collectionFormat;

        // create the params based on the collection format
        if ("multi".equals(format)) {
            return values.stream()
                    .map(value -> new Pair(urlEncode(name), urlEncode(valueToString(value))))
                    .collect(Collectors.toList());
        }

        String delimiter;
        switch (format) {
            case "csv":
                delimiter = urlEncode(",");
                break;
            case "ssv":
                delimiter = urlEncode(" ");
                break;
            case "tsv":
                delimiter = urlEncode("\t");
                break;
            case "pipes":
                delimiter = urlEncode("|");
                break;
            default:
                throw new IllegalArgumentException("Illegal collection format: " + collectionFormat);
        }

        StringJoiner joiner = new StringJoiner(delimiter);
        for (Object value : values) {
            joiner.add(urlEncode(valueToString(value)));
        }

        return Collections.singletonList(new Pair(urlEncode(name), joiner.toString()));
    }

    public static HttpResponse.BodyHandler<String> withProblemSupportAndStringResponse() {
        return (responseInfo) -> {
            int statusCode = responseInfo.statusCode();
            if (statusCode == 200) {
                return HttpResponse.BodyHandlers.ofString().apply(responseInfo);
            } else {
                HttpResponse.BodySubscriber<String> bodySubscriber = HttpResponse.BodySubscribers.replacing(null);
                bodySubscriber.onError(new ConfigurationValueServerHttpStatusException(
                        statusCode,
                        String.format("HTTP request failed with response status code %s", statusCode)));
                return bodySubscriber;
            }
        };
    }

}
