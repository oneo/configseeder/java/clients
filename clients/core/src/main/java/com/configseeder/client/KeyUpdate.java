/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import com.configseeder.client.model.ConfigValue;
import lombok.Data;

/**
 * Update notification details.
 */
@Data
public class KeyUpdate {

    /** The key of the value. */
    private final String key;

    /** The previous value. */
    private final ConfigValue previousValue;

    /** The new value. */
    private final ConfigValue newValue;

    /** {@code true} if value has been deleted. */
    private final boolean addedValue;

    /** {@code true} if value has been deleted. */
    private final boolean deletedValue;

}
