/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.util;

import static java.util.Collections.emptyMap;

import com.configseeder.client.Logger;
import com.configseeder.client.model.ConfigValue;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Map;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RecoveryFileConfigurationRepository {

    static final String DEFAULT_FILENAME = ".configseeder-recovery.json";

    private static final Logger logger = Logger.getLogger(RecoveryFileConfigurationRepository.class);

    private final JsonValueMapper jsonValueMapper;

    private File recoveryFile = null;

    /**
     * Sets path where the recovery file should be written to.
     */
    public void setRecoveryFilePath(String recoveryFilePath) {
        if (recoveryFilePath == null) {
            recoveryFile = null;
        } else {
            recoveryFile = createDirectoryAndFile(recoveryFilePath);
        }
    }

    /**
     * Gets recovery file path.
     */
    public String getRecoveryFilePath() {
        return recoveryFile == null ? null : recoveryFile.getAbsolutePath();
    }

    /**
     * Reads persisted values for the configured recovery file.
     */
    public Map<String, ConfigValue> getPersistedValues() {
        if (recoveryFile == null) {
            return emptyMap();
        }
        try {
            String fileContent = new String(Files.readAllBytes(recoveryFile.toPath()));
            return jsonValueMapper.parse(fileContent);
        } catch (IOException e) {
            logger.warn("Unable to read contents of " + recoveryFile.getAbsolutePath(), e);
            return emptyMap();
        }
    }

    /**
     * Saves current configuration values to the recovery file. File will be overridden.
     *
     * @param values values to store
     */
    public void saveConfigValues(Collection<ConfigValue> values) {
        if (recoveryFile == null) {
            return;
        }

        String jsonString = jsonValueMapper.write(values);
        try {
            Files.write(recoveryFile.toPath(), jsonString.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            logger.warn("Unable to write contents to " + recoveryFile.getAbsolutePath(), e);
        }
    }

    private File createDirectoryAndFile(String recoveryFilePath) {
        File file = new File(recoveryFilePath);
        if (!file.exists()) {
            if (recoveryFilePath.endsWith("/")) {
                // create folder recursively
                try {
                    Files.createDirectories(file.toPath());
                } catch (IOException ex) {
                    logger.warn("Unable to create directory '" + file.toPath() + "'", ex);
                    return null;
                }
                file = new File(file.getAbsoluteFile(), DEFAULT_FILENAME);
            }
            return createFile(file);
        }
        if (file.isDirectory()) {
            file = new File(file.getAbsolutePath(), DEFAULT_FILENAME);
            if (!file.exists()) {
                return createFile(file);
            }
        }
        return file;
    }

    private File createFile(File file) {
        try {
            String directory = file.getParent();
            if (directory != null && !createDirectory(directory)) {
                return null;
            }
            if (file.createNewFile()) {
                logger.info("Recovery file " + file.getAbsolutePath() + " created.");
                return file;
            } else {
                logger.warn("Recovery file " + file.getAbsolutePath() + " could not be created");
                return null;
            }
        } catch (IOException e) {
            logger.error("Unable to create recoveryFile " + file.getAbsolutePath());
            return null;
        }
    }

    private boolean createDirectory(String directory) {
        try {
            Files.createDirectories(Paths.get(directory));
        } catch (IOException ex) {
            logger.warn("Unable to create directory '" + directory + "'", ex);
            return false;
        }
        return true;
    }

}
