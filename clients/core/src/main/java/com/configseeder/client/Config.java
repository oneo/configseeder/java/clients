/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import com.configseeder.client.model.Identification;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * Simple static config provider.
 */
public class Config {

    private static final Logger logger = new Logger(Config.class);
    private static Config config;
    private final Properties localConfig;
    private final ConfigSeederClient configClient;

    /**
     * Expects config seeder and local configs to be placed in application.config.
     */
    private Config() {
        this("/application.config", "/application.config");
    }

    /**
     * Setup a config from an already initialized config client.
     *
     * @param configClient configuration client
     */
    private Config(ConfigSeederClient configClient) {
        this.localConfig = new Properties();
        this.configClient = configClient;
    }

    /**
     * Customize configuration location for your local config and remote config.
     *
     * @param configFileName local config
     * @param configSeederConfigFileName remote config
     */
    private Config(String configFileName, String configSeederConfigFileName) {
        localConfig = new Properties();
        try {
            final InputStream applicationPropertiesStream = Config.class.getResourceAsStream(configFileName);
            localConfig.load(applicationPropertiesStream);
        } catch (IOException e) {
            logger.info("No application.properties found. Ignoring");
        }

        final ConfigSeederClientConfiguration configSeederClientConfiguration = ConfigSeederClientConfiguration.fromProperties(configSeederConfigFileName);
        configClient = new ConfigSeederClient(configSeederClientConfiguration, Identification.create("Core"));
    }

    /**
     * Setup config based on an existing config seeder client.
     *
     * @param configClient config client
     * @return config
     */
    public static Config init(ConfigSeederClient configClient) {
        config = new Config(configClient);
        return config;
    }

    /**
     * Setups custom config instance.
     *
     * @param configFileName config file name
     * @param configSeederConfigFileName remote config file name.
     * @return new instance
     */
    public static Config setupCustomInstance(String configFileName, String configSeederConfigFileName) {
        config = new Config(configFileName, configSeederConfigFileName);
        return config;
    }

    private String getLocalValue(String key) {
        return localConfig.getProperty(key);
    }

    private String getRemoteValue(String key) {
        return configClient.getString(key);
    }

    /**
     * Get config instance.
     * @return instance of an application config
     */
    private static Config getInstance() {
        if (config == null) {
            config = new Config();
        }
        return config;
    }

    /**
     * Loads a String value for a given key.
     *
     * @param key key
     * @return value
     */
    private String getValue(String key) {
        final String localValue = getLocalValue(key);
        if (localValue != null) {
            return localValue;
        }
        return getRemoteValue(key);
    }

    /**
     * Expects a value to be configured for the given key.
     *
     * @param key key
     * @return value
     */
    public static String getString(String key) {
        final String value = getInstance().getValue(key);
        if (value == null) {
            throw new IllegalStateException("Configuration for key '" + key + "' does not exist");
        }
        return value;
    }

    /**
     * Returns a string if configured, otherwise falls back to the fallback.
     *
     * @param key key
     * @param fallback fallback
     * @return value
     */
    public static String getString(String key, String fallback) {
        final String value = getInstance().getValue(key);
        return value == null ? fallback : value;
    }

    /**
     * If a string is configured, returns the value.
     *
     * @param key key
     * @return value
     */
    public static Optional<String> getOptionalString(String key) {
        return Optional.ofNullable(getInstance().getValue(key));
    }

    /**
     * Expects a long number to exists on the given key.
     *
     * @param key key
     * @return value
     */
    public static Long getLong(String key) {
        return Long.valueOf(getString(key));
    }

    /**
     * Returns the configured long value, otherwise falls back to the fallback.
     *
     * @param key key
     * @param fallback fallback value
     * @return the value
     */
    public static Long getLong(String key, Long fallback) {
        final String value = getInstance().getValue(key);
        return value == null ? fallback : Long.valueOf(value);
    }

    /**
     * If key exists, expects to be a long value.
     *
     * @param key key
     * @return the value
     */
    public static Optional<Long> getOptionalLong(String key) {
        final String value = getInstance().getValue(key);
        return Optional.ofNullable(value).map(Long::valueOf);
    }

    /**
     * Expects a boolean to be configured and returns the value.
     *
     * @param key key
     * @return value
     */
    public static Boolean getBoolean(String key) {
        return Boolean.valueOf(getString(key));
    }

    /**
     * Get boolean with fallback if null.
     *
     * @param key key
     * @param fallback fallback value
     * @return the boolean
     */
    public static boolean getBoolean(String key, boolean fallback) {
        final String value = getInstance().getValue(key);
        return value == null ? fallback : Boolean.parseBoolean(value);
    }

    /**
     * Returns a boolean if exists.
     *
     * @param key key
     * @return the boolean
     */
    public static Optional<Boolean> getOptionalBoolean(String key) {
        final String value = getInstance().getValue(key);
        return Optional.ofNullable(value).map(Boolean::valueOf);
    }


    /**
     * Lists all keys available.
     *
     * @return all available keys
     */
    public Set<String> getKeys() {
        final Set<String> allKeys = new TreeSet<>();
        localConfig.keySet().forEach(entry -> allKeys.add(String.valueOf(entry)));
        allKeys.addAll(configClient.getKeys());
        return allKeys;
    }

}
