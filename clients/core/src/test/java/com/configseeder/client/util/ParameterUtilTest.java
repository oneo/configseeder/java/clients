/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class ParameterUtilTest {

    @Test
    void shouldSnakeWell() {
        assertThat("HELLO_WORLD").isEqualTo(ParameterUtil.snakeCase("hello.world"));
        assertThat("HELLOWORLD").isEqualTo(ParameterUtil.snakeCase("hello-world"));
        assertThat("HELLOWORLD").isEqualTo(ParameterUtil.snakeCase("helloWorld"));
        assertThat("MY_PARAMETERCASENOW").isEqualTo(ParameterUtil.snakeCase("my.parameter-caseNow"));
    }

    @Test
    void shouldGetFromEnvironment() {
        // given
        System.setProperty("env-test", " value-with-space\n");

        // when
        final String result = ParameterUtil.getParameter("env-test");

        // then
        assertThat(result).isEqualTo("value-with-space");
    }

}