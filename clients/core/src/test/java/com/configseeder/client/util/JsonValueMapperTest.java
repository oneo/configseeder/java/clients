/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.util;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import com.configseeder.client.model.ConfigValue;
import com.configseeder.client.model.ConfigurationValueType;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;
import org.junit.jupiter.api.Test;

class JsonValueMapperTest {

    final JsonValueMapper mapper = new JsonValueMapper();

    @Test
    void shouldMapAndBeEqual() {
        // given
        ConfigValue value1 = new ConfigValue();
        value1.setKey("key.a");
        value1.setValue("value-a");
        value1.setType(ConfigurationValueType.STRING);
        value1.setLastChange(OffsetDateTime.of(2019, 5, 21, 17, 0, 5, 0, ZoneOffset.UTC));
        value1.setLastChangeInMilliseconds(value1.getLastChange().toInstant().toEpochMilli());

        // when
        String content = mapper.write(singletonList(value1));
        Map<String, ConfigValue> values = mapper.parse(content);

        // then
        assertThat(values.get("key.a")).isNotNull();
        assertThat(values.get("key.a")).isEqualTo(value1);
    }

    @Test
    void shouldMapWithNullValues() {
        // given
        JsonObject object = Json.object();

        // when
        ConfigValue configValue = mapper.mapConfigValue(object);

        // then
        assertThat(configValue.getValue()).isNull();
        assertThat(configValue.getKey()).isNull();
        assertThat(configValue.getLastChangeInMilliseconds()).isNotNegative();
        assertThat(configValue.getLastChange()).isNotNull();
        assertThat(configValue.getType()).isEqualTo(ConfigurationValueType.STRING);
    }

    @Test
    void shouldMapWithMostlyNullValues() {
        // given
        JsonObject object = Json.object();
        object.add("key", (String) null);
        object.add("value", (String) null);
        object.add("type", (String) null);
        object.add("lastUpdateInMilliseconds", (String) null);
        object.add("lastUpdate", (String) null);

        // when
        ConfigValue configValue = mapper.mapConfigValue(object);

        // then
        assertThat(configValue.getValue()).isNull();
        assertThat(configValue.getKey()).isNull();
        assertThat(configValue.getLastChangeInMilliseconds()).isNotNegative();
        assertThat(configValue.getLastChange()).isNotNull();
        assertThat(configValue.getType()).isEqualTo(ConfigurationValueType.STRING);
    }


    @Test
    void shouldMapDateTimeNull() {
        assertThat(mapper.mapZonedDateTime(mapper.mapOffsetDateTime((String) null))).isNull();
        assertThat(mapper.mapOffsetDateTime(mapper.mapZonedDateTime((OffsetDateTime) null))).isNull();
    }

    @Test
    void shouldMapLocalDateToDateTime() {
        assertThat(mapper.mapOffsetDateTime("2020-05-21T00:00:00")).isBefore(OffsetDateTime.of(2020, 5, 22, 0, 0, 0, 0, ZoneOffset.UTC));
    }

}