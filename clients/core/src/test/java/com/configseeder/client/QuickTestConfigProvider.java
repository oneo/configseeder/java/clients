/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client;

import static java.util.Collections.singletonList;

import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.Identification;
import com.configseeder.client.model.VersionedConfigurationGroupSelectionMode;

/**
 * Integration test for {@link ConfigSeederClient}.
 */
public class QuickTestConfigProvider {

    public static void main(String[] args) throws InterruptedException {
        // given
        final String serverUrl = "http://localhost:8080";
        final String apiKey = "eyJraWQiOiIxMjMiLCJhbGciOiJSUzUxMiJ9.eyJ0eXBlIjoiYXBpa2V5IiwidHlwZSI6ImFwaS1rZXkiLCJpc3MiOiJPTkVPIENvbmZpZyBTZXJ2ZXIiLCJpYXQiOjE1Mjg2OTM2MzIsImV4cCI6MTU0NjI5NzIwMCwic3ViIjoiY3ViZXN0YXRpb24iLCJhcGktdG9rZW4taWQiOiI3ZjVmYjU0NC05MzE4LTRlMTMtYjkyYy1iNDMyOWQ1MTcxYWMiLCJhY2Nlc3MiOnsiZW52aXJvbm1lbnRzIjpbeyJpZCI6IjUwOGMxMDY2LTE4MWUtNDczYi1hNzlhLWExN2I1MjIwYjQzZCIsImtleSI6IkNJIn0seyJpZCI6IjI0N2M5OGY2LTRjNTgtNDJjMi05NDMyLTI0ODU2NWQ3YWQzMiIsImtleSI6Ik5JRyJ9LHsiaWQiOiJkMDFkNjI5NC0zNzczLTQzM2MtYThlOC1kMDg0Y2FiNTcyNmIiLCJrZXkiOiJSRUwifSx7ImlkIjoiYTYyYWNiMjktOWU0MC00MzAyLTlkMzktZjM0MmRmZDAxNzU0Iiwia2V5IjoiSU5UIn0seyJpZCI6ImY4MjQyMTdhLTFjNjUtNDcxMC04NTNlLTQ1YTliYmQwNTAxZCIsImtleSI6IlRFU1QifSx7ImlkIjoiMmMwOWIzMDEtNGUyNi00OGQxLWEzNDctOTU4M2M0YTJiOTNhIiwia2V5IjoiUFJPRCJ9XSwiY29uZmlndXJhdGlvbi1ncm91cHMiOlt7ImlkIjoiNzEzYjFiMzUtNTA1OC00YzE2LWFhZjItYjRlYzkzMzMwNjZjIiwia2V5IjoiQ3ViZSJ9XX19.LvgyjcRpHQnrxUpQpcFOKKeWGwmV-YgBXllsuCSMSe_hLmXsfHaU2R21ZOtOO3bYJ_Sq1blFpz1gIQRK54gAyzfvrqypXZ3PR2gm-H-Cfl_9J_E3tS7QjlJiRgbf13_f7IFExJJZuB7JdLR-1fYHESCDVEy3zoeT8hQwe9iiB-A";
        final String tenantKey = "SL";
        final String environmentKey = "PROD";
        final String configurationGroupKey = "cubestation";
        final RefreshMode refreshMode = RefreshMode.TIMER;
        final InitializationMode initializationMode = InitializationMode.LAZY;
        final ConfigSeederClientConfiguration providerConfiguration = ConfigSeederClientConfiguration.builder()
                .serverUrl(serverUrl)
                .apiKey(apiKey)
                .requestConfiguration(ConfigurationRequest.builder()
                        .tenantKey(tenantKey)
                        .environmentKey(environmentKey)
                        .configurations(singletonList(ConfigurationEntryRequest.builder()
                                .configurationGroupKey(configurationGroupKey)
                                .build()))
                        .build())
                .refreshMode(refreshMode)
                .initializationMode(initializationMode)
                .build();

        // when
        final ConfigSeederClient configuration = new ConfigSeederClient(providerConfiguration, Identification.create("Test"));

        // request values each 2 seconds * 10
        for (int i = 0; i < 10; i++) {
            System.out.println(configuration.getString("cube.scheduler"));
            Thread.sleep(2000); // NOSONAR : This is expected here to just show updates each 2 seconds.
        }
    }

}
