/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.client.maven;

import com.configseeder.client.ConfigSeederClient;
import com.configseeder.client.ConfigSeederClientConfiguration;
import com.configseeder.client.ConfigSeederClientConfiguration.ConfigSeederClientConfigurationBuilder;
import com.configseeder.client.InitializationMode;
import com.configseeder.client.RefreshMode;
import com.configseeder.client.model.ConfigurationEntryRequest;
import com.configseeder.client.model.ConfigurationRequest;
import com.configseeder.client.model.Identification;
import lombok.Getter;
import lombok.Setter;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Getter
@Setter
@Mojo(name = "fetch", defaultPhase = LifecyclePhase.INITIALIZE, threadSafe = true)
public class ConfigSeederValuePlugin extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    @Parameter(property = "serverUrl", required = true)
    private String serverUrl;

    @Parameter(property = "apiKey", required = true)
    private String apiKey;

    @Parameter(property = "tenant", required = true)
    private String tenant;

    @Parameter(property = "environment", required = true)
    private String environment;

    @Parameter(property = "configurationGroups", required = true)
    private List<ConfigurationGroupMavenConfiguration> configurationGroups;

    @Parameter(property = "dateTime")
    private String dateTime;

    @Parameter(property = "version")
    private String version;

    @Deprecated
    @Parameter(property = "context")
    private String context;

    @Parameter(property = "labels")
    private List<String> labels;

    @Parameter(property = "refreshCycle")
    private Integer refreshCycle;

    @Parameter(property = "retryWaitTime")
    private Integer retryWaitTime;

    @Parameter(property = "maxRetries")
    private Integer maxRetries;

    @Parameter(property = "timeout")
    private Integer timeout;

    @Override
    public void execute() throws MojoFailureException {
        final ConfigSeederClientConfiguration configSeederClientConfiguration = fetchConfiguration();
        try {
            configSeederClientConfiguration.validate();
        } catch (IllegalArgumentException e) {
            throw new MojoFailureException(e.getMessage());
        }

        final Properties projectProperties = project.getProperties();

        final ConfigSeederClient configProvider = new ConfigSeederClient(configSeederClientConfiguration, Identification.create("Maven"));
        final Map<String, String> properties = configProvider.getProperties();
        if (!configProvider.isInitialized()) {
            throw new MojoFailureException("Could not fetch values from configuration server");
        }
        getLog().info("Provide properties:");
        properties.forEach((key, value) -> {
            getLog().info("- " + key);
            projectProperties.put(key, value);
        });
    }

    private ConfigSeederClientConfiguration fetchConfiguration() {
        final List<ConfigurationEntryRequest> configGroups = this.configurationGroups == null
                ? emptyList()
                : this.configurationGroups.stream()
                        .map(cg -> ConfigurationEntryRequest.builder()
                                .configurationGroupKey(cg.getKey())
                                .build())
                        .collect(Collectors.toList());

        final ConfigurationRequest configurationRequest = ConfigurationRequest.builder()
                .environmentKey(environment)
                .tenantKey(tenant)
                .dateTime(dateTime == null || dateTime.isBlank() ? null : LocalDateTime.parse(dateTime))
                .version(version)
                .labels(labels != null && !labels.isEmpty() ? labels : (context == null ? null : List.of(context)))
                .configurations(configGroups)
                .build();

        final ConfigSeederClientConfigurationBuilder builder = ConfigSeederClientConfiguration.builder()
                .serverUrl(serverUrl)
                .apiKey(apiKey)
                .initializationMode(InitializationMode.EAGER)
                .refreshMode(RefreshMode.MANUAL)
                .requestConfiguration(configurationRequest);
        if (refreshCycle != null) {
            builder.refreshCycle(refreshCycle);
        }
        if (retryWaitTime != null) {
            builder.retryWaitTime(retryWaitTime);
        }
        if (maxRetries != null) {
            builder.maxRetries(maxRetries);
        }
        if (timeout != null) {
            builder.timeout(Duration.ofMillis(timeout));
        }
        return builder.build();
    }

}
