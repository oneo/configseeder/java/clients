/*
 * Copyright (c) 2022 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.client.smallryeconfig;

import static com.configseeder.client.ConfigSeederClientConfiguration.ALL_CONFIG_KEYS;

import com.configseeder.client.ConfigSeederClientConfiguration;
import com.configseeder.client.microprofileconfig.ConfigSeederClientBaseConfigurationProvider;
import com.configseeder.client.microprofileconfig.KeyValue;
import io.smallrye.config.ConfigSourceContext;
import java.io.InputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;
import lombok.extern.java.Log;

@Log
public class ConfigSeederClientConfigurationProvider extends ConfigSeederClientBaseConfigurationProvider {

    public ConfigSeederClientConfiguration getClientConfiguration(ConfigSourceContext configSourceContext) {
        InputStream configseederYaml = ConfigSeederClientConfigurationProvider.class.getResourceAsStream("/configseeder.yaml");
        if (configseederYaml != null) {
            return createConfigurationFromYaml();
        } else {
            return createConfiguration(configSourceContext);
        }
    }

    public ConfigSeederClientConfiguration createConfiguration(ConfigSourceContext configSourceContext) {
        log.log(Level.FINER, "Create ConfigSeederClientConfiguration based on ConfigSourceContext");
        final Map<Object, Object> existingConfigs = ALL_CONFIG_KEYS.stream()
                .map(k -> new KeyValue(k, configSourceContext.getValue(k).getValue()))
                .filter(p -> p.getValue() != null)
                .collect(Collectors.toMap(KeyValue::getKey, KeyValue::getValue));
        return ConfigSeederClientConfiguration.fromMap(existingConfigs);
    }

}
