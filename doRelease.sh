#!/bin/bash

COLOR=`tput setaf 3`
NC=`tput sgr0`

echo "${COLOR}Start release process. Please wait while fetching checking for next version.${NC}";

# Get the current mvn version
MVN_VERSION=$(mvn -q \
    -Dexec.executable=echo \
    -Dexec.args='${project.version}' \
    --non-recursive \
    exec:exec)
MVN_VERSION=${MVN_VERSION/-SNAPSHOT/}
CURRENT_BRANCH=$(git branch | grep \* | cut -d ' ' -f2)

read -p "${COLOR}Release version ($MVN_VERSION):${NC} " releaseVersion
read -p "${COLOR}Next version:${NC} " nextVersion

if [ -z "$releaseVersion" ]; then
  releaseVersion="$MVN_VERSION";
fi

read -p "${COLOR}OK to release ${releaseVersion} with next release ${nextVersion} ? (Y/n)${NC} " ok
if [ -z "$ok" ] || [ "$ok" = "y" ]; then
  echo "READY"
else
  exit;
fi

echo "${COLOR}Start release with maven ${NC}"
mvn -P examples release:clean release:prepare -Dtag=${releaseVersion} release:prepare -DreleaseVersion=${releaseVersion} -DdevelopmentVersion=${nextVersion} -Darguments="-DskipTests" -DskipTests || exit 1

echo "${COLOR}Push changes to origin ${NC}"
git push || exit 1

PREVIOUS_COMMIT=$(git rev-list -n 1 HEAD^1)
echo "${COLOR}Set main to the previous commit ${PREVIOUS_COMMIT} ${NC}"
git branch -f main ${PREVIOUS_COMMIT} || exit 1

echo "${COLOR}Checkout main${NC}"
git checkout main || exit 1

echo "${COLOR}Push main${NC}"
git push || exit 1

echo "Full build and deploy artifacts"
mvn deploy || exit 1

echo "${COLOR}Set branch back to original: ${CURRENT_BRANCH} ${NC}"
git checkout ${CURRENT_BRANCH} || exit 1

echo "${COLOR}DONE! ${NC}"
