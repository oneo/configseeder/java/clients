# ConfigSeeder® Java Demo

This repository contains couple of Java Demo Examples, how *ConfigSeeder®* can be used for your Java applications.

There are examples based on:
- SpringBoot (Spring Integration)
- Quarkus or Thorntail (Eclipse Microprofile Config Integration)
- Java FX (*ConfigSeeder®* Java Core)
- Maven (Maven Integration)

All demo applications are working by default against https://demo.configseeder.com/. Use user `democlient` with password `democlient` to modify the values.

For support and documentation, visit the official website located at https://configseeder.com/

## Demo Tutorial

### Step 1: Play with ConfigSeeder®

> Goal: Play with JavaFX Client which is connected to the Demo Instance. Play with the values.

#### Preconditions 1

1. Java installed to your computer with JavaFX Support (https://openjdk.java.net/install/)
2. *ConfigSeeder®* Demo JavaFX Client Downloaded from https://download.configseeder.com/ > Demo JavaFX download

#### Workshop 1

1. Start JavaFX Client with: `java -jar configseeder-demo-javafx-<version>.jar`
2. Login to https://demo.configseeder.com/ with user `democlient` and password `democlient`.
3. Open `SampleConfig` Configuration Group
4. For example change the color (`background.color`) to another color (e.g. for red use `#990000`)
5. Now the JavaFX Client should update the background color after few seconds to the configured color.

### Step 2: Check other integration samples

> Goal: Run Spring or Quarkus Application against *ConfigSeeder®* Demo Instance.

#### Preconditions 2

1. Demo Code cloned to your machine with: `git clone https://gitlab.com/oneo/configseeder/java/demo.git`
2. Code compiled with maven with `mvn clean install`

#### Workshop 2

1. Start your desired demo app within your IDE:
   - **JavaFX Client**:
     1. Start main class `com.configseeder.demo.fx.DemoFxClient`
   - **Spring**:
     1. Start main class `com.configseeder.demo.swisslife.SpringDemoApplication`
     2. Check values on http://localhost:8082/
   - **Quarkus**:
     1. Start `mvn compile quarkus:dev`
     2. Check values on http://localhost:8081/
1. Check if your changes on the https://demo.configseeder.com/ instance are shown in the different demo applications (For Spring and Quarkus demo just refresh the browser).

### Step 3: Run your own ConfigSeeder® instance.

> Goal: You are able to run your own *ConfigSeeder®* instance and create a new configuration group and an api key.

#### Preconditions 3

1. Download the latest H2 instance of *ConfigSeeder®* from https://download.configseeder.com/
2. Start instance with `java -jar configseeder-server-management-app-h2-<version>.jar`
3. Open your Browser on http://localhost:8080/

#### Workshop 3

1. Ensure there is an environment called `DEV` (on `/ui/environments`). If needed create it:
   -  Short Name: `DEV`
   -  Save
2. Create a new configuration group for your application (on `/ui/groups/`)
   - Key: `MyApp`
   - Enabled Filter Columns:
     - Disable `Version`
     - Disable `Context`
   - Save
3. Create a couple of key / values and assign it to environment `DEV`
4. Create an API key to access the configuration group and the environment (on `/ui/api-keys/`). Click on create:
   - Name: `MyAppKey`
   - Configuration Groups: `MyApp`
   - Environment: `DEV`
   - Validity: in the future ;-)
   - Save
   - Copy and save the API Key for the next Workshop

### Step 4: Connect Demo Instances to your local ConfigSeeder®

> Goal: You are able to connect any other application to your local *ConfigSeeder®*.

#### Workshop 4

1. Create `configseeder.yml` with following content and replace `{apiKey}` with the values from above.

    ```yaml
    configseeder:
      client:
        refreshCycle: 5000
        serverUrl: "http://localhost:8080/"
        apiKey: "{apiKey}"
        configurationGroupKeys: "MyApp"
        environmentKey: "DEV"
        tenantKey: "default"
    ```

2. Start the JavaFX Demo application with `java -Dconfigseeder.client.file=~/path/to/configseeder.yml -jar configseeder-demo-javafx.jar`
3. If you like to connect also the Spring or Quarkus Demo app to your instance, just adapt the `<component>/src/main/resources/configseeder.yml` according to your needs. More configuration options can be found on the documentation page: https://docs.configseeder.com/management/latest/systemdocumentation/index.html#configseeder-client-configs
4. Restart your desired demo app with your IDE:

   - **JavaFX Client**:
     1. Start main class `com.configseeder.demo.fx.DemoFxClient`
   - **Spring**:
     1. Start main class `com.configseeder.demo.swisslife.SpringDemoApplication`
     2. Check values on http://localhost:8082/
   - **Quarkus**:
     1. Start `mvn compile quarkus:dev`
     2. Check instructions for [Quarkus](./quarkus)
   - **Quarkus**:
     1. Start `mvn compile quarkus:dev`
     2. Check values on http://localhost:8081/

In production scenario, you should *never* store API Keys as part of your application, but pass it to your application either by
environment variable or parameter. We will try the same thing now.

1. Remove from `configseeder.yml` the `apiKey` configuration.
2. In your start script pass the API Key with `-Dconfigseeder.client.apiKey=....` and check if you can still connect.
3. This can also be done using environment variables. Create an environment variable `CONFIGSEEDER_CLIENT_APIKEY` with the API Key. Start your application and you should notice it's still working.

> Well done. You have completed our workshop and you know the basics how to integrate an applicatio with *ConfigSeeder*!