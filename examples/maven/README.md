# ConfigSeeder Maven Integration

## Start

* Start maven with demo profile: `mvn -Pdemo validate`

* => You should see values in `target/classes/filtered.properties` as defined on config seeder
