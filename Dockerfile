FROM debian:buster-slim

MAINTAINER Daniel Kellenberger daniel.kellenberger@oneo.ch

ARG MAVEN_VERSION=3.6.3
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

# Install libraries used for builds
RUN apt-get update \
    && apt-get upgrade -y \
    && mkdir -p /usr/share/man/man1 \
    && apt-get install -y --no-install-recommends \
        software-properties-common \
	    wget \
        gnupg \
        curl \
        ca-certificates \
        bzip2 \
        git \
        zip \
        unzip \
        lsof \
        graphviz \
        libncurses5 \
        git \
	&& rm -rf /var/cache/apt/archives/* \
	&& rm -rf /var/lib/apt/lists/*

# Install Zulu 11
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9 \
    && apt-add-repository 'deb http://repos.azulsystems.com/debian stable main' \
    && apt-get update \
    && apt-get install -y --no-install-recommends zulu-11

# Install Maven
RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && rm -f /usr/bin/mvn \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

# Add JavaFX
RUN curl -fsSL -o /tmp/javafx-11-0-2-sdk-linux.zip http://gluonhq.com/download/javafx-11-0-2-sdk-linux/ \
  && cd /tmp/ \
  && unzip /tmp/javafx-11-0-2-sdk-linux.zip \
  && cp -arf /tmp/javafx-sdk-11.0.2/lib/* /usr/lib/jvm/zulu-11-amd64/lib/